﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace TransactionSolution
{
    public class DataController
    {
        public static IRestResponse PutApiTransaction(Dictionary<string,string> queryParameters
                                              ,string urlApi
                                              ,string routeApi
                                              ,Method method)
        {
            RestClient restClient = new RestClient(urlApi);
            RestRequest restRequest = new RestRequest(routeApi, method);

            if (queryParameters != null)
            {
                foreach (var parameter in queryParameters)
                {
                    if (!string.IsNullOrEmpty(parameter.Value))
                    {
                        restRequest.AddQueryParameter(parameter.Key, parameter.Value);
                    }
                }
            }

            IRestResponse responseResult = restClient.Execute(restRequest);

            if (responseResult == null || responseResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var errorMessage = responseResult.Content ?? responseResult.ErrorMessage ?? "Erro ao executar API";
                throw new Exception(errorMessage);

            }
            return responseResult;
        }

        public static CustomerStructPersistence LoadConfig(string path, string file)
        {
            string contentJson = File.ReadAllText(path+ file);

            CustomerStructPersistence jsonReturn = JsonSerializer.Deserialize<CustomerStructPersistence>(contentJson);

            return jsonReturn;
        }

        public static string BuiltInsertQuery(string name
                                              , string rfid
                                              , string cpf
                                              , string endereco
                                              , string nascimento
                                              , string numero
                                              , string genero)
        {
            string query = DbQueryTransactions.InsertRegisterPeoble.Replace("@Nome",name)
                                                                   .Replace("@Rfid", rfid)
                                                                   .Replace("@Cpf", cpf)
                                                                   .Replace("@Endereco", endereco)
                                                                   .Replace("@Nascimento", nascimento)
                                                                   .Replace("@Numero", numero)
                                                                   .Replace("@Genero", genero);
            return query;

        }
    }
}
