﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionSolution
{
    public class DbQueryTransactions
    {
		public static string HtmlDevices = @"Select
												Cust.Customer As CustmCode
												,Cust.[Description] As CustmDesc
												,Devc.Device AS DeviceCode
												,Devc.[Description] As DeviceDesc
												,isnull(Sum(Flux.Id),0) As TransactionQty
												,Max(Flux.[Action]) As LastAction
												,Max(Flux.TransactionDate) As LastDateProcess
												,isnull(Max(Flux.ProcessTime),0) As LastTimeProcess
											From
												CustomerConfigIntegrationData Cust With(Nolock)
											Inner Join
												CustomerDevicesIntegrationData Devc With(Nolock)
											On
												Cust.Id = Devc.IdCustomer
											Left Join
												FlowIntegrationData Flux With(Nolock)
											On
													Cust.Id = Flux.IdCustomer
												And Devc.Id = Flux.IdDevice
											Where
												Cust.Id = @IdCustomer
											Group By
												Cust.Customer
												,Cust.[Description]
												,Devc.Device
												,Devc.[Description]	";


		public static string InsertNewDevice = @"
												Insert 
													CustomerDevicesIntegrationData
													(
														IdCustomer
													   ,Device
													   ,[Description]
													)
												Values
													(
														@IdCustomer
													   ,'@Device'
													   ,'@Description'
												)";

		public static string InsertRegisterPeoble = @"
														Insert into 
															CadastroPessoas
															(
															 Nome
															 ,Rfid
															 ,Cpf
															 ,Endereco
															 ,Nascimento
															 ,Numero
															 ,Genero
															)
														Values
															(
															  '@Nome'
															 ,'@Rfid'
															 ,'@Cpf'
															 ,'@Endereco'
															 ,'@Nascimento'
															 ,'@Numero'
															 ,'@Genero'
															)
													";


	}
}
