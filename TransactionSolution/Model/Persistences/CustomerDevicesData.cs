﻿using Sequor.DB.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionSolution
{
    public class CustomerDevicesData
    {
        public string custmCode { get; set; }
        public string custmDesc { get; set; }
        public string deviceCode { get; set; }
        public string deviceDesc { get; set; }
        public string transactionQty { get; set; }
        public string lastAction { get; set; }
        public string lastDateProcess { get; set; }
        public string lastTimeProcess { get; set; }
    }
    public class Content
    {
        public List<CustomerDevicesData> content { get; set; }
    }
}
