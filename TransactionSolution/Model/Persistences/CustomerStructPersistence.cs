﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionSolution
{
    public class CustomerStructPersistence
    {
        public int IdCustomer { get; set; }
        public string ConnectionTransactionStringMaster { get; set; }
        public string ConnectionTransactionStringSlave { get; set; }

        public string ConnectionClientStringMaster { get; set; }
        public string ConnectionClientStringSlave { get; set; }
    }
}
