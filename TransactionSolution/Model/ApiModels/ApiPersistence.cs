﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionSolution
{
    public enum ScriptAction
    {
        Consulta,
        Manipulacao
    }

    public class  ApiPersistence
    {
        public int IdCustomer { get; set; }
        public string ScriptTransaction { get; set; } = string.Empty;
        public ScriptAction Action { get; set; }
        public DateTime TransactionDate { get; set; }
        public int IdDevice { get; set; }
    }
}
