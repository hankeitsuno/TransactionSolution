﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionSolution
{
    public class DeviceModelStruct
    {

        public string htmlCurrentDevices { get; set; } = @"<!DOCTYPE html>
                            <html lang='pt-BR'>
                            <head>
                                <meta charset = 'UTF-8'>
                                <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                                <title>Lista de Dispositivos</title>
                                <style>
                               body {
            font-family: 'Arial', sans-serif;
            background-color: #f2f2f2;
        }

        .contributor {
            background-color: #fff;
            padding: 20px;
            margin: 20px;
            border-radius: 5px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.2);
        }

        h1 {
            color: #3498db; /* Azul */
        }

        .device-list {
            list-style: none;
            padding: 0;
        }

        .device-item {
            margin: 5px;
            padding: 5px;
            border-radius: 5px;
            cursor: pointer;
            transition: background-color 0.3s ease;
            background-color: #3498db; /* Azul */
            color: #fff; /* Texto branco */
        }

        /* Cor de fundo quando ativo */
        .device-item.active {
            background-color: #2980b9; /* Azul mais escuro */
        }
                                </style>
                            </head>
                            <body>
                                <div class='contributor'>                             
                                     <h1>{Contribute}</h1>                                
                                            {Device}
                                </div>                            
                                <script>
                                    const deviceItems = document.querySelectorAll('.device-item');                            
                            deviceItems.forEach(item => {
                                const details = item.querySelector('.device-details');                            
                                item.addEventListener('click', () => {
                                    item.classList.toggle('active');
                                });
                            });
                                </script>
                            </body>
                            </html>";    
    }
}
