﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TransactionSolution
{
    public class UcManager : UserControl
    {
        UCGenericPainel ucGeneralPanel = null;
        UcDevices ucDevices = new UcDevices { Dock = DockStyle.Fill };
        UcManipulateTransactions ucManTransactions = new UcManipulateTransactions { Dock = DockStyle.Fill };
        public const string PathConfig = @"C:\!!ESTUDOS\TCC\TransactionSolution\TransactionSolution\Model\ConfigStruct";
        public const string ConfigFile = @"\ConfiCustomer.json";
        private DevExpress.XtraEditors.TileItem tileSend;
        CustomerStructPersistence Config = new CustomerStructPersistence();

        public UcManager()
        {
            InitializeComponent();
            AgregateDependenceUc();
        }

        public void AgregateDependenceUc()
        {
            ucGeneralPanel = new UCGenericPainel { Dock = DockStyle.Fill };
            this.tblManager.Controls.Add(ucGeneralPanel, 1,0);
        }

        public void  FillDataCostumer()
        {
            Config = DataController.LoadConfig(PathConfig, ConfigFile);
        }

        public void VisibleCurrentUc(UserControl currentUc)
        {
            ucGeneralPanel.AgregateUC(currentUc, Config);
            ucGeneralPanel.Visible = true;
            ucGeneralPanel.BringToFront();
        }
        private void tileSend_ItemClick(object sender, DevExpress.XtraEditors.TileItemEventArgs e)
        {
            VisibleCurrentUc(ucManTransactions);
            ucGeneralPanel.SetBtnText("Enviar");
        }

        private void tileDevices_ItemClick(object sender, DevExpress.XtraEditors.TileItemEventArgs e)
        {
            ucGeneralPanel.SetBtnText("Novo");
            VisibleCurrentUc(ucDevices);
            ucDevices.LoadHtml(Config);
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            this.pnlManager = new System.Windows.Forms.Panel();
            this.tblFooter = new System.Windows.Forms.TableLayoutPanel();
            this.tblManager = new System.Windows.Forms.TableLayoutPanel();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileDevices = new DevExpress.XtraEditors.TileItem();
            this.tileSend = new DevExpress.XtraEditors.TileItem();
            this.pnlManager.SuspendLayout();
            this.tblFooter.SuspendLayout();
            this.tblManager.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlManager
            // 
            this.pnlManager.BackColor = System.Drawing.Color.Transparent;
            this.pnlManager.Controls.Add(this.tblFooter);
            this.pnlManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlManager.Location = new System.Drawing.Point(0, 0);
            this.pnlManager.Name = "pnlManager";
            this.pnlManager.Size = new System.Drawing.Size(1171, 619);
            this.pnlManager.TabIndex = 0;
            // 
            // tblFooter
            // 
            this.tblFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(230)))));
            this.tblFooter.ColumnCount = 1;
            this.tblFooter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblFooter.Controls.Add(this.tblManager, 0, 0);
            this.tblFooter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblFooter.Location = new System.Drawing.Point(0, 0);
            this.tblFooter.Name = "tblFooter";
            this.tblFooter.RowCount = 2;
            this.tblFooter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.38068F));
            this.tblFooter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.61932F));
            this.tblFooter.Size = new System.Drawing.Size(1171, 619);
            this.tblFooter.TabIndex = 0;
            // 
            // tblManager
            // 
            this.tblManager.ColumnCount = 2;
            this.tblManager.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.02575F));
            this.tblManager.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.97425F));
            this.tblManager.Controls.Add(this.tileControl1, 0, 0);
            this.tblManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblManager.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblManager.Location = new System.Drawing.Point(3, 3);
            this.tblManager.Name = "tblManager";
            this.tblManager.RowCount = 1;
            this.tblManager.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblManager.Size = new System.Drawing.Size(1165, 547);
            this.tblManager.TabIndex = 0;
            // 
            // tileControl1
            // 
            this.tileControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tileControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(230)))));
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.HorizontalContentAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tileControl1.ItemBorderVisibility = DevExpress.XtraEditors.TileItemBorderVisibility.Never;
            this.tileControl1.Location = new System.Drawing.Point(3, 3);
            this.tileControl1.MaxId = 2;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tileControl1.Size = new System.Drawing.Size(203, 541);
            this.tileControl1.TabIndex = 0;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Top;
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.tileDevices);
            this.tileGroup2.Items.Add(this.tileSend);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileDevices
            // 
            this.tileDevices.AllowAnimation = false;
            this.tileDevices.AllowSelectAnimation = false;
            this.tileDevices.AppearanceItem.Normal.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.tileDevices.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tileDevices.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileDevices.AppearanceItem.Normal.Options.UseFont = true;
            tileItemElement1.AnchorAlignment = DevExpress.Utils.AnchorAlignment.Top;
            tileItemElement1.Text = "Dispositivos";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.tileDevices.Elements.Add(tileItemElement1);
            this.tileDevices.Id = 0;
            this.tileDevices.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileDevices.Name = "tileDevices";
            this.tileDevices.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileDevices_ItemClick);
            // 
            // tileSend
            // 
            this.tileSend.AllowAnimation = false;
            this.tileSend.AllowSelectAnimation = false;
            this.tileSend.AppearanceItem.Normal.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.tileSend.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.tileSend.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileSend.AppearanceItem.Normal.Options.UseFont = true;
            tileItemElement2.Text = "Enviar Dados";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.tileSend.Elements.Add(tileItemElement2);
            this.tileSend.Id = 1;
            this.tileSend.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileSend.Name = "tileSend";
            this.tileSend.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileSend_ItemClick);
            // 
            // UcManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(243)))), ((int)(((byte)(254)))));
            this.Controls.Add(this.pnlManager);
            this.Name = "UcManager";
            this.Size = new System.Drawing.Size(1171, 619);
            this.pnlManager.ResumeLayout(false);
            this.tblFooter.ResumeLayout(false);
            this.tblManager.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlManager;
        private System.Windows.Forms.TableLayoutPanel tblFooter;
        private System.Windows.Forms.TableLayoutPanel tblManager;
        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem tileDevices;


    }
}
