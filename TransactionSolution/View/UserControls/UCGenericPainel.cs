﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TransactionSolution.View.UserControls.Device;

namespace TransactionSolution
{
    public partial class UCGenericPainel : UserControl
    {
        UcRegisterNewDevices ucRegister = null;
        UcManipulateTransactions ucTransactions;
        CustomerStructPersistence Config = new CustomerStructPersistence();
        public UCGenericPainel()
        {
            InitializeComponent();
            this.Visible = false;
        }

        public void AgregateUC(UserControl currentUc, CustomerStructPersistence config)
        {
            Config = config;
            try
            {
                ucTransactions = (UcManipulateTransactions)currentUc;
                ucTransactions.GetConfig(Config);
            }

            catch { }
            this.tblGeneralPanel.Controls[0].Visible = false;
            this.tblGeneralPanel.Controls.Add(currentUc, 0, 1);            
            currentUc.Visible = true;
            currentUc.BringToFront();
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public void SetBtnText(string btnText)
        {
            this.tleRight.Text = btnText;
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            this.pnlGeneral = new System.Windows.Forms.Panel();
            this.tblGeneralPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tleRight = new DevExpress.XtraEditors.TileItem();
            this.pnlGeneral.SuspendLayout();
            this.tblGeneralPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlGeneral
            // 
            this.pnlGeneral.Controls.Add(this.tblGeneralPanel);
            this.pnlGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGeneral.Location = new System.Drawing.Point(0, 0);
            this.pnlGeneral.Name = "pnlGeneral";
            this.pnlGeneral.Size = new System.Drawing.Size(833, 559);
            this.pnlGeneral.TabIndex = 0;
            // 
            // tblGeneralPanel
            // 
            this.tblGeneralPanel.ColumnCount = 1;
            this.tblGeneralPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblGeneralPanel.Controls.Add(this.lblTitle, 0, 0);
            this.tblGeneralPanel.Controls.Add(this.tileControl1, 0, 2);
            this.tblGeneralPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblGeneralPanel.Location = new System.Drawing.Point(0, 0);
            this.tblGeneralPanel.Name = "tblGeneralPanel";
            this.tblGeneralPanel.RowCount = 3;
            this.tblGeneralPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tblGeneralPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 430F));
            this.tblGeneralPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tblGeneralPanel.Size = new System.Drawing.Size(833, 559);
            this.tblGeneralPanel.TabIndex = 0;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Location = new System.Drawing.Point(3, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(827, 8);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tileControl1
            // 
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.ItemSize = 100;
            this.tileControl1.Location = new System.Drawing.Point(3, 411);
            this.tileControl1.MaxId = 2;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Size = new System.Drawing.Size(827, 294);
            this.tileControl1.TabIndex = 1;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.tleRight);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tleRight
            // 
            this.tleRight.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(64)))));
            this.tleRight.AppearanceItem.Normal.Font = new System.Drawing.Font("Trebuchet MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tleRight.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tleRight.AppearanceItem.Normal.Options.UseFont = true;
            tileItemElement16.Text = "Novo";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.tleRight.Elements.Add(tileItemElement16);
            this.tleRight.Id = 1;
            this.tleRight.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tleRight.Name = "tleRight";
            this.tleRight.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tleRight_ItemClick);
            // 
            // UCGenericPainel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Controls.Add(this.pnlGeneral);
            this.Name = "UCGenericPainel";
            this.Size = new System.Drawing.Size(833, 559);
            this.pnlGeneral.ResumeLayout(false);
            this.tblGeneralPanel.ResumeLayout(false);
            this.tblGeneralPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlGeneral;
        private System.Windows.Forms.TableLayoutPanel tblGeneralPanel;
        private System.Windows.Forms.Label lblTitle;
        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem tleRight;

        private void tleRight_ItemClick(object sender, DevExpress.XtraEditors.TileItemEventArgs e)
        {
            if (this.tleRight.Text != "Registrar" && this.tleRight.Text != "Enviar")
            {
                ucRegister = new UcRegisterNewDevices { Dock = DockStyle.Fill };
                this.tblGeneralPanel.Controls[0].Visible = false;
                this.tblGeneralPanel.Controls.Add(ucRegister, 0, 1);
                ucRegister.Visible = true;
                ucRegister.BringToFront();
                this.tleRight.Text = "Registrar";
            }
            else if (this.tleRight.Text == "Enviar")
            {
                ucTransactions.SetStatus("Enviando dados preenchidos...", Color.Yellow);

                ucTransactions.SendData();
            }
            else
            {
                ucRegister.RegisterNewDevice(Config);
            }
        }

    }
}
