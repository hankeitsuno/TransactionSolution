﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TransactionSolution
{
    public partial class UcDevices : UserControl
    {
        DeviceModelStruct modelStructDevice = null;

        public UcDevices()
        {
            InitializeComponent();
            modelStructDevice = new DeviceModelStruct();
        }

        public void LoadHtml(CustomerStructPersistence config)
        {
            brwHtml.DocumentText = LoadHtmlData(config);
            brwHtml.Dock = DockStyle.Fill;
            brwHtml.ScriptErrorsSuppressed = true;
            brwHtml.Focus();
        }

        private string LoadHtmlData(CustomerStructPersistence config)
        {
            Content jsonContent = new Content();

            Dictionary<string, string> parametersApi = new Dictionary<string, string>()
                                         {
                                          {"IdCustomer",config.IdCustomer.ToString()},
                                          {"ScriptTransaction",DbQueryTransactions.HtmlDevices.Replace("@IdCustomer",config.IdCustomer.ToString())},
                                          {"ConnectionStringMaster",config.ConnectionTransactionStringMaster},
                                          {"ConnectionStringSlave",config.ConnectionTransactionStringSlave},
                                          {"Action","0"},
                                          {"TransactionDate",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")},
                                          {"IdDevice","1"},
                                         };

            IRestResponse apiResult = DataController.PutApiTransaction(parametersApi, ApiConsts.ApiTransactionUrl,ApiConsts.TranscationRoutine,Method.PUT);

            if (apiResult.StatusCode == System.Net.HttpStatusCode.OK && apiResult.Content != null)    
                    jsonContent = JsonSerializer.Deserialize<Content>(apiResult.Content);

            return BuiltHtml(jsonContent, modelStructDevice.htmlCurrentDevices);
          
        }

        private string BuiltHtml(Content htmlContent ,string htmlStruct)
        {
            int count = 0;
            string devices = null;
            for (int i = 0; i < htmlContent.content.Count; i++)
                devices += @"<ul class='device-list'>{Device}"+i+ @"

                             <li class='device-item'>{DeviceContent}"+ i + @"</li>
                                </ul>";


            htmlStruct = htmlStruct.Replace("{Device}", devices);

            foreach (var content in htmlContent.content)
            {
                htmlStruct = htmlStruct.Replace("{Contribute}", content.custmCode + " | " + content.custmDesc);
                htmlStruct = htmlStruct.Replace("{Device}" + count, content.deviceDesc);
                htmlStruct = htmlStruct.Replace("{DeviceContent}"+ count, "Código: " + (content.deviceCode ?? "-") +
                                                                        " | Quantidade de transações: " + (content.transactionQty ?? "-") +
                                                                        " | Úlima transação: " + (content.lastAction ?? "-") +
                                                                        " | Data da ultima transação: " + (content.lastDateProcess ?? "-") +
                                                                        " | Tempo de execução: " + (content.lastTimeProcess ?? "-")+ " ms");
                count++;
            }

            return htmlStruct;
        }     


        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.brwHtml = new System.Windows.Forms.WebBrowser();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(230)))));
            this.panel1.Controls.Add(this.brwHtml);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(965, 524);
            this.panel1.TabIndex = 0;
            // 
            // brwHtml
            // 
            this.brwHtml.Dock = System.Windows.Forms.DockStyle.Fill;
            this.brwHtml.Location = new System.Drawing.Point(0, 0);
            this.brwHtml.MinimumSize = new System.Drawing.Size(20, 20);
            this.brwHtml.Name = "brwHtml";
            this.brwHtml.Size = new System.Drawing.Size(965, 524);
            this.brwHtml.TabIndex = 0;
            // 
            // UcDevices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "UcDevices";
            this.Size = new System.Drawing.Size(965, 524);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.WebBrowser brwHtml;
    }
}
