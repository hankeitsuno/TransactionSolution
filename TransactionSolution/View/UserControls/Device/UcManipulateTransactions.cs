﻿using Bogus;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TransactionSolution
{
    public partial class UcManipulateTransactions : UserControl
    {
        private System.Windows.Forms.Timer timer1;
        private TableLayoutPanel tblHeader;
        private DevExpress.XtraEditors.LabelControl lblTitleNewDevice;
        private DevExpress.XtraEditors.CheckButton btnAuto;
        private TableLayoutPanel tblIdCustomer;
        private Label lblName;
        private TextBox txtName;
        private TableLayoutPanel tblScriptTransaction;
        private Label lblRfid;
        private TextBox txtRfid;
        private TableLayoutPanel tblConMaster;
        private Label label4;
        private TextBox txtCpf;
        private TableLayoutPanel tblConSlave;
        private Label label5;
        private TextBox txtEndereco;
        private TableLayoutPanel tblAction;
        private TextBox txtBirth;
        private Label label6;
        private TableLayoutPanel tblIdDevice;
        private Label label1;
        private TextBox txtNumber;
        private TableLayoutPanel tableLayoutPanel1;
        private Label lblGender;
        private ListBox txtGender;
        private DevExpress.XtraEditors.LabelControl lblStatus;
        private TableLayoutPanel tblPnlPrincipal;
        public CustomerStructPersistence config = new CustomerStructPersistence();
        public UcManipulateTransactions()
        {
            InitializeComponent();
        }

        private void btnAuto_CheckedChanged(object sender, EventArgs e)
        {

            if (this.btnAuto.Checked)
            {
                SetStatus("Enviando dados automaticos(10) aguarde...", Color.Yellow);

                timer1.Start();
            }
            else
            {
                SetStatus("Todos os dados foram enviados com sucesso", Color.Green);

                timer1.Stop();
                timer1.Dispose();
            }
        }

        
        private void timer1_Tick(object sender, EventArgs e)
        {
            int maxinsert = 10;

            for (int i = 0; i < maxinsert; i++)
            {
                var faker = new Faker("pt_BR");
                SendData(faker.Name.FullName(), faker.Person.Random.Guid().ToString(), faker.Person.Address.ZipCode, faker.Person.Address.State, faker.Person.DateOfBirth.ToString(), faker.Phone.PhoneNumber(), faker.Person.Gender.ToString());
            }

            this.btnAuto.Checked = false;
        }

        public void GetConfig(CustomerStructPersistence config)
        {
            this.config = config;
        }

        public void SendData(string name
                             , string rfid
                             , string cpf
                             , string endereco
                             , string nascimento
                             , string numero
                            , string genero)
        {

            string scriptTransaction = DataController.BuiltInsertQuery(name, rfid, cpf, endereco, nascimento, numero,genero);

            Dictionary<string, string> parametersApi = new Dictionary<string, string>()
                                         {
                                          {"IdCustomer", config.IdCustomer.ToString()},
                                          {"ScriptTransaction",scriptTransaction},
                                          {"ConnectionStringMaster",config.ConnectionClientStringMaster},
                                          {"ConnectionStringSlave",config.ConnectionClientStringSlave},
                                          {"Action","1"},
                                          {"TransactionDate",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")},
                                          {"IdDevice","1"},
                                         };

            DataController.PutApiTransaction(parametersApi, ApiConsts.ApiTransactionUrl, ApiConsts.TranscationRoutine, Method.PUT);

        }

        public void SendData()
        {
           string scriptTransaction =  DataController.BuiltInsertQuery(this.txtName.Text,this.txtRfid.Text,this.txtCpf.Text
                                                                      ,this.txtEndereco.Text,this.txtBirth.Text,this.txtNumber.Text
                                                                      ,this.txtGender.Text.Trim());

            Dictionary<string, string> parametersApi = new Dictionary<string, string>()
                                         {
                                          {"IdCustomer", config.IdCustomer.ToString()},
                                          {"ScriptTransaction",scriptTransaction},
                                          {"ConnectionStringMaster",config.ConnectionClientStringMaster},
                                          {"ConnectionStringSlave",config.ConnectionClientStringSlave},
                                          {"Action","1"},
                                          {"TransactionDate",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")},
                                          {"IdDevice","1"},
                                         };

            DataController.PutApiTransaction(parametersApi, ApiConsts.ApiTransactionUrl, ApiConsts.TranscationRoutine, Method.PUT);
            SetStatus("Dados enviados com sucesso", Color.Green);

            ClearTexts();
        }

        public void ClearTexts()
        {
            this.txtName.Clear();
            this.txtBirth.Clear();
            this.txtCpf.Clear();
            this.txtEndereco.Clear();
            this.txtGender.ClearSelected();
            this.txtNumber.Clear();
            this.txtRfid.Clear();
        }
        public void SetStatus(string messageStatus, Color color)
        {
            this.lblStatus.Text = messageStatus;
            this.lblStatus.ForeColor = color;
        }
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tblHeader = new System.Windows.Forms.TableLayoutPanel();
            this.btnAuto = new DevExpress.XtraEditors.CheckButton();
            this.lblTitleNewDevice = new DevExpress.XtraEditors.LabelControl();
            this.tblIdCustomer = new System.Windows.Forms.TableLayoutPanel();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.tblScriptTransaction = new System.Windows.Forms.TableLayoutPanel();
            this.txtRfid = new System.Windows.Forms.TextBox();
            this.lblRfid = new System.Windows.Forms.Label();
            this.tblConMaster = new System.Windows.Forms.TableLayoutPanel();
            this.txtCpf = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tblConSlave = new System.Windows.Forms.TableLayoutPanel();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tblAction = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBirth = new System.Windows.Forms.TextBox();
            this.tblIdDevice = new System.Windows.Forms.TableLayoutPanel();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtGender = new System.Windows.Forms.ListBox();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblStatus = new DevExpress.XtraEditors.LabelControl();
            this.tblPnlPrincipal = new System.Windows.Forms.TableLayoutPanel();
            this.tblHeader.SuspendLayout();
            this.tblIdCustomer.SuspendLayout();
            this.tblScriptTransaction.SuspendLayout();
            this.tblConMaster.SuspendLayout();
            this.tblConSlave.SuspendLayout();
            this.tblAction.SuspendLayout();
            this.tblIdDevice.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tblPnlPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tblHeader
            // 
            this.tblHeader.ColumnCount = 2;
            this.tblHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.59371F));
            this.tblHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.40629F));
            this.tblHeader.Controls.Add(this.lblTitleNewDevice, 0, 0);
            this.tblHeader.Controls.Add(this.btnAuto, 1, 0);
            this.tblHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblHeader.Font = new System.Drawing.Font("Trebuchet MS", 27.75F);
            this.tblHeader.Location = new System.Drawing.Point(3, 3);
            this.tblHeader.Name = "tblHeader";
            this.tblHeader.RowCount = 1;
            this.tblHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblHeader.Size = new System.Drawing.Size(827, 44);
            this.tblHeader.TabIndex = 0;
            // 
            // btnAuto
            // 
            this.btnAuto.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 27.75F);
            this.btnAuto.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(64)))));
            this.btnAuto.Appearance.Options.UseFont = true;
            this.btnAuto.Appearance.Options.UseForeColor = true;
            this.btnAuto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAuto.Location = new System.Drawing.Point(561, 3);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(263, 38);
            this.btnAuto.TabIndex = 0;
            this.btnAuto.Text = "Automatico";
            this.btnAuto.CheckedChanged += new System.EventHandler(this.btnAuto_CheckedChanged);
            // 
            // lblTitleNewDevice
            // 
            this.lblTitleNewDevice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTitleNewDevice.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblTitleNewDevice.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleNewDevice.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(64)))));
            this.lblTitleNewDevice.Appearance.Options.UseBackColor = true;
            this.lblTitleNewDevice.Appearance.Options.UseFont = true;
            this.lblTitleNewDevice.Appearance.Options.UseForeColor = true;
            this.lblTitleNewDevice.LineLocation = DevExpress.XtraEditors.LineLocation.Center;
            this.lblTitleNewDevice.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTitleNewDevice.Location = new System.Drawing.Point(155, 3);
            this.lblTitleNewDevice.Name = "lblTitleNewDevice";
            this.lblTitleNewDevice.Size = new System.Drawing.Size(248, 46);
            this.lblTitleNewDevice.TabIndex = 1;
            this.lblTitleNewDevice.Text = "Envio de dados";
            // 
            // tblIdCustomer
            // 
            this.tblIdCustomer.ColumnCount = 2;
            this.tblIdCustomer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblIdCustomer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblIdCustomer.Controls.Add(this.lblName, 0, 0);
            this.tblIdCustomer.Controls.Add(this.txtName, 1, 0);
            this.tblIdCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblIdCustomer.Location = new System.Drawing.Point(3, 53);
            this.tblIdCustomer.Name = "tblIdCustomer";
            this.tblIdCustomer.RowCount = 1;
            this.tblIdCustomer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblIdCustomer.Size = new System.Drawing.Size(827, 34);
            this.tblIdCustomer.TabIndex = 3;
            // 
            // txtName
            // 
            this.txtName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtName.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.ForeColor = System.Drawing.Color.Black;
            this.txtName.Location = new System.Drawing.Point(416, 3);
            this.txtName.Multiline = true;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(408, 28);
            this.txtName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblName.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.Black;
            this.lblName.Location = new System.Drawing.Point(3, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(407, 34);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Nome";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tblScriptTransaction
            // 
            this.tblScriptTransaction.ColumnCount = 2;
            this.tblScriptTransaction.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblScriptTransaction.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblScriptTransaction.Controls.Add(this.lblRfid, 0, 0);
            this.tblScriptTransaction.Controls.Add(this.txtRfid, 1, 0);
            this.tblScriptTransaction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblScriptTransaction.Location = new System.Drawing.Point(3, 93);
            this.tblScriptTransaction.Name = "tblScriptTransaction";
            this.tblScriptTransaction.RowCount = 1;
            this.tblScriptTransaction.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblScriptTransaction.Size = new System.Drawing.Size(827, 34);
            this.tblScriptTransaction.TabIndex = 4;
            // 
            // txtRfid
            // 
            this.txtRfid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRfid.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRfid.ForeColor = System.Drawing.Color.Black;
            this.txtRfid.Location = new System.Drawing.Point(416, 3);
            this.txtRfid.Multiline = true;
            this.txtRfid.Name = "txtRfid";
            this.txtRfid.Size = new System.Drawing.Size(408, 28);
            this.txtRfid.TabIndex = 1;
            // 
            // lblRfid
            // 
            this.lblRfid.AutoSize = true;
            this.lblRfid.BackColor = System.Drawing.Color.Transparent;
            this.lblRfid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRfid.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold);
            this.lblRfid.ForeColor = System.Drawing.Color.Black;
            this.lblRfid.Location = new System.Drawing.Point(3, 0);
            this.lblRfid.Name = "lblRfid";
            this.lblRfid.Size = new System.Drawing.Size(407, 34);
            this.lblRfid.TabIndex = 0;
            this.lblRfid.Text = "Rfid";
            this.lblRfid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tblConMaster
            // 
            this.tblConMaster.ColumnCount = 2;
            this.tblConMaster.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblConMaster.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblConMaster.Controls.Add(this.label4, 0, 0);
            this.tblConMaster.Controls.Add(this.txtCpf, 1, 0);
            this.tblConMaster.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblConMaster.Location = new System.Drawing.Point(3, 133);
            this.tblConMaster.Name = "tblConMaster";
            this.tblConMaster.RowCount = 1;
            this.tblConMaster.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblConMaster.Size = new System.Drawing.Size(827, 34);
            this.tblConMaster.TabIndex = 5;
            // 
            // txtCpf
            // 
            this.txtCpf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCpf.Font = new System.Drawing.Font("Trebuchet MS", 12F);
            this.txtCpf.ForeColor = System.Drawing.Color.Black;
            this.txtCpf.Location = new System.Drawing.Point(416, 3);
            this.txtCpf.Multiline = true;
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(408, 28);
            this.txtCpf.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(407, 34);
            this.label4.TabIndex = 0;
            this.label4.Text = "Cpf";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tblConSlave
            // 
            this.tblConSlave.ColumnCount = 2;
            this.tblConSlave.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblConSlave.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblConSlave.Controls.Add(this.label5, 0, 0);
            this.tblConSlave.Controls.Add(this.txtEndereco, 1, 0);
            this.tblConSlave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblConSlave.Location = new System.Drawing.Point(3, 173);
            this.tblConSlave.Name = "tblConSlave";
            this.tblConSlave.RowCount = 1;
            this.tblConSlave.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblConSlave.Size = new System.Drawing.Size(827, 34);
            this.tblConSlave.TabIndex = 6;
            // 
            // txtEndereco
            // 
            this.txtEndereco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEndereco.Font = new System.Drawing.Font("Trebuchet MS", 12F);
            this.txtEndereco.ForeColor = System.Drawing.Color.Black;
            this.txtEndereco.Location = new System.Drawing.Point(416, 3);
            this.txtEndereco.Multiline = true;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(408, 28);
            this.txtEndereco.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(407, 34);
            this.label5.TabIndex = 0;
            this.label5.Text = "Endereco";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tblAction
            // 
            this.tblAction.ColumnCount = 2;
            this.tblAction.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblAction.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblAction.Controls.Add(this.txtBirth, 0, 0);
            this.tblAction.Controls.Add(this.label6, 0, 0);
            this.tblAction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblAction.Location = new System.Drawing.Point(3, 213);
            this.tblAction.Name = "tblAction";
            this.tblAction.RowCount = 1;
            this.tblAction.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblAction.Size = new System.Drawing.Size(827, 34);
            this.tblAction.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(407, 34);
            this.label6.TabIndex = 0;
            this.label6.Text = "Nascimento";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBirth
            // 
            this.txtBirth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBirth.Font = new System.Drawing.Font("Trebuchet MS", 12F);
            this.txtBirth.ForeColor = System.Drawing.Color.Black;
            this.txtBirth.Location = new System.Drawing.Point(416, 3);
            this.txtBirth.Multiline = true;
            this.txtBirth.Name = "txtBirth";
            this.txtBirth.Size = new System.Drawing.Size(408, 28);
            this.txtBirth.TabIndex = 2;
            // 
            // tblIdDevice
            // 
            this.tblIdDevice.ColumnCount = 2;
            this.tblIdDevice.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblIdDevice.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblIdDevice.Controls.Add(this.label1, 0, 0);
            this.tblIdDevice.Controls.Add(this.txtNumber, 1, 0);
            this.tblIdDevice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblIdDevice.Location = new System.Drawing.Point(3, 253);
            this.tblIdDevice.Name = "tblIdDevice";
            this.tblIdDevice.RowCount = 1;
            this.tblIdDevice.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblIdDevice.Size = new System.Drawing.Size(827, 34);
            this.tblIdDevice.TabIndex = 8;
            // 
            // txtNumber
            // 
            this.txtNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNumber.Font = new System.Drawing.Font("Trebuchet MS", 12F);
            this.txtNumber.ForeColor = System.Drawing.Color.Black;
            this.txtNumber.Location = new System.Drawing.Point(416, 3);
            this.txtNumber.Multiline = true;
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(408, 28);
            this.txtNumber.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(407, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblGender, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtGender, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 293);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(827, 79);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // txtGender
            // 
            this.txtGender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGender.Font = new System.Drawing.Font("Trebuchet MS", 12F);
            this.txtGender.FormattingEnabled = true;
            this.txtGender.ItemHeight = 22;
            this.txtGender.Items.AddRange(new object[] {
            "  Male",
            "  Female",
            "  Another"});
            this.txtGender.Location = new System.Drawing.Point(416, 3);
            this.txtGender.Name = "txtGender";
            this.txtGender.Size = new System.Drawing.Size(408, 73);
            this.txtGender.TabIndex = 1;
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.BackColor = System.Drawing.Color.Transparent;
            this.lblGender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGender.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold);
            this.lblGender.ForeColor = System.Drawing.Color.Black;
            this.lblGender.Location = new System.Drawing.Point(3, 0);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(407, 79);
            this.lblGender.TabIndex = 0;
            this.lblGender.Text = "Genero";
            this.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStatus
            // 
            this.lblStatus.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Appearance.Options.UseFont = true;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblStatus.Location = new System.Drawing.Point(3, 378);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 22);
            this.lblStatus.TabIndex = 10;
            // 
            // tblPnlPrincipal
            // 
            this.tblPnlPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tblPnlPrincipal.ColumnCount = 1;
            this.tblPnlPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblPnlPrincipal.Controls.Add(this.lblStatus, 0, 8);
            this.tblPnlPrincipal.Controls.Add(this.tableLayoutPanel1, 0, 7);
            this.tblPnlPrincipal.Controls.Add(this.tblIdDevice, 0, 6);
            this.tblPnlPrincipal.Controls.Add(this.tblAction, 0, 5);
            this.tblPnlPrincipal.Controls.Add(this.tblConSlave, 0, 4);
            this.tblPnlPrincipal.Controls.Add(this.tblConMaster, 0, 3);
            this.tblPnlPrincipal.Controls.Add(this.tblScriptTransaction, 0, 2);
            this.tblPnlPrincipal.Controls.Add(this.tblIdCustomer, 0, 1);
            this.tblPnlPrincipal.Controls.Add(this.tblHeader, 0, 0);
            this.tblPnlPrincipal.Location = new System.Drawing.Point(0, 0);
            this.tblPnlPrincipal.Name = "tblPnlPrincipal";
            this.tblPnlPrincipal.RowCount = 9;
            this.tblPnlPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tblPnlPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblPnlPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblPnlPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblPnlPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblPnlPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblPnlPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblPnlPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tblPnlPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tblPnlPrincipal.Size = new System.Drawing.Size(833, 559);
            this.tblPnlPrincipal.TabIndex = 0;
            // 
            // UcManipulateTransactions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.Controls.Add(this.tblPnlPrincipal);
            this.Name = "UcManipulateTransactions";
            this.Size = new System.Drawing.Size(833, 559);
            this.tblHeader.ResumeLayout(false);
            this.tblHeader.PerformLayout();
            this.tblIdCustomer.ResumeLayout(false);
            this.tblIdCustomer.PerformLayout();
            this.tblScriptTransaction.ResumeLayout(false);
            this.tblScriptTransaction.PerformLayout();
            this.tblConMaster.ResumeLayout(false);
            this.tblConMaster.PerformLayout();
            this.tblConSlave.ResumeLayout(false);
            this.tblConSlave.PerformLayout();
            this.tblAction.ResumeLayout(false);
            this.tblAction.PerformLayout();
            this.tblIdDevice.ResumeLayout(false);
            this.tblIdDevice.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tblPnlPrincipal.ResumeLayout(false);
            this.tblPnlPrincipal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
