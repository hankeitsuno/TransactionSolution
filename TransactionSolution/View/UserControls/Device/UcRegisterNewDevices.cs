﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TransactionSolution.View.UserControls.Device
{
    public partial class UcRegisterNewDevices : UserControl
    {
        public UcRegisterNewDevices()
        {
            InitializeComponent();
        }

        public void RegisterNewDevice(CustomerStructPersistence config)
        {

            if (string.IsNullOrEmpty(txtDevice.Text) || string.IsNullOrEmpty(txtDesc.Text))
            {
                lblStatus.Text = "Preencha os campos corretamente";
                lblStatus.ForeColor = Color.Red;

            }
            else
            {
                Dictionary<string, string> parametersApi = new Dictionary<string, string>()
                                         {
                                          {"IdCustomer",config.IdCustomer.ToString()},
                                          {"ScriptTransaction",DbQueryTransactions.InsertNewDevice.Replace("@IdCustomer","2")
                                                                                  .Replace("@Device",txtDevice.Text)
                                                                                  .Replace("@Description",txtDesc.Text)},
                                          {"ConnectionStringMaster",config.ConnectionTransactionStringMaster},
                                          {"ConnectionStringSlave",config.ConnectionTransactionStringSlave},
                                          {"Action","1"},
                                          {"IdDevice","1"},
                                         };

                DataController.PutApiTransaction(parametersApi, ApiConsts.ApiTransactionUrl, ApiConsts.TranscationRoutine, Method.PUT);

                lblStatus.Text = "Novo dispositivo cadastrado com sucesso";
                lblStatus.ForeColor = Color.Green;
            }
        }
    }
}
