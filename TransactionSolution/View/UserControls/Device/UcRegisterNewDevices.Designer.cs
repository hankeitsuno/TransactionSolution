﻿
namespace TransactionSolution.View.UserControls.Device
{
    partial class UcRegisterNewDevices
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tblDesc = new System.Windows.Forms.TableLayoutPanel();
            this.lblDescription = new DevExpress.XtraEditors.LabelControl();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.tblDevice = new System.Windows.Forms.TableLayoutPanel();
            this.lblDevice = new DevExpress.XtraEditors.LabelControl();
            this.txtDevice = new System.Windows.Forms.TextBox();
            this.lblTitleNewDevice = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblStatus = new DevExpress.XtraEditors.LabelControl();
            this.tblDesc.SuspendLayout();
            this.tblDevice.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tblDesc
            // 
            this.tblDesc.ColumnCount = 2;
            this.tblDesc.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblDesc.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblDesc.Controls.Add(this.lblDescription, 0, 0);
            this.tblDesc.Controls.Add(this.txtDesc, 1, 0);
            this.tblDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblDesc.Location = new System.Drawing.Point(3, 304);
            this.tblDesc.Name = "tblDesc";
            this.tblDesc.RowCount = 1;
            this.tblDesc.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblDesc.Size = new System.Drawing.Size(959, 95);
            this.tblDesc.TabIndex = 2;
            // 
            // lblDescription
            // 
            this.lblDescription.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(64)))));
            this.lblDescription.Appearance.Options.UseFont = true;
            this.lblDescription.Appearance.Options.UseForeColor = true;
            this.lblDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDescription.Location = new System.Drawing.Point(3, 3);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(473, 89);
            this.lblDescription.TabIndex = 0;
            this.lblDescription.Text = "Descrição do dispositivo : ";
            // 
            // txtDesc
            // 
            this.txtDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDesc.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(184)))), ((int)(((byte)(191)))));
            this.txtDesc.Location = new System.Drawing.Point(482, 3);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(474, 35);
            this.txtDesc.TabIndex = 1;
            // 
            // tblDevice
            // 
            this.tblDevice.ColumnCount = 2;
            this.tblDevice.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblDevice.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblDevice.Controls.Add(this.lblDevice, 0, 0);
            this.tblDevice.Controls.Add(this.txtDevice, 1, 0);
            this.tblDevice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblDevice.Location = new System.Drawing.Point(3, 203);
            this.tblDevice.Name = "tblDevice";
            this.tblDevice.RowCount = 1;
            this.tblDevice.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblDevice.Size = new System.Drawing.Size(959, 95);
            this.tblDevice.TabIndex = 1;
            // 
            // lblDevice
            // 
            this.lblDevice.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDevice.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(64)))));
            this.lblDevice.Appearance.Options.UseFont = true;
            this.lblDevice.Appearance.Options.UseForeColor = true;
            this.lblDevice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDevice.Location = new System.Drawing.Point(3, 3);
            this.lblDevice.Name = "lblDevice";
            this.lblDevice.Size = new System.Drawing.Size(473, 89);
            this.lblDevice.TabIndex = 0;
            this.lblDevice.Text = "Codigo do dispositivo : ";
            // 
            // txtDevice
            // 
            this.txtDevice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDevice.Font = new System.Drawing.Font("Trebuchet MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDevice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(184)))), ((int)(((byte)(191)))));
            this.txtDevice.Location = new System.Drawing.Point(482, 3);
            this.txtDevice.Name = "txtDevice";
            this.txtDevice.Size = new System.Drawing.Size(474, 35);
            this.txtDevice.TabIndex = 1;
            // 
            // lblTitleNewDevice
            // 
            this.lblTitleNewDevice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTitleNewDevice.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(230)))));
            this.lblTitleNewDevice.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleNewDevice.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(64)))));
            this.lblTitleNewDevice.Appearance.Options.UseBackColor = true;
            this.lblTitleNewDevice.Appearance.Options.UseFont = true;
            this.lblTitleNewDevice.Appearance.Options.UseForeColor = true;
            this.lblTitleNewDevice.LineLocation = DevExpress.XtraEditors.LineLocation.Center;
            this.lblTitleNewDevice.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.lblTitleNewDevice.Location = new System.Drawing.Point(243, 77);
            this.lblTitleNewDevice.Name = "lblTitleNewDevice";
            this.lblTitleNewDevice.Size = new System.Drawing.Size(479, 46);
            this.lblTitleNewDevice.TabIndex = 0;
            this.lblTitleNewDevice.Text = "Cadastro de Novo dispositivo";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(220)))), ((int)(((byte)(230)))));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lblTitleNewDevice, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tblDevice, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tblDesc, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(965, 524);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblStatus
            // 
            this.lblStatus.Appearance.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Appearance.Options.UseFont = true;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblStatus.Location = new System.Drawing.Point(3, 405);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 22);
            this.lblStatus.TabIndex = 3;
            // 
            // UcRegisterNewDevices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UcRegisterNewDevices";
            this.Size = new System.Drawing.Size(965, 524);
            this.tblDesc.ResumeLayout(false);
            this.tblDesc.PerformLayout();
            this.tblDevice.ResumeLayout(false);
            this.tblDevice.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tblDesc;
        private DevExpress.XtraEditors.LabelControl lblDescription;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.TableLayoutPanel tblDevice;
        private DevExpress.XtraEditors.LabelControl lblDevice;
        private System.Windows.Forms.TextBox txtDevice;
        private DevExpress.XtraEditors.LabelControl lblTitleNewDevice;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl lblStatus;
    }
}
